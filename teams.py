from random import shuffle
from math import floor


class Teams:

    def __init__(self, num_of_teams, names):  # num_of_team = int, names = list
        self.names = names
        self.num_of_teams = num_of_teams
        self.teams = []
        self.players_per_team = floor(len(self.names) / self.num_of_teams)
        self.remaining_players = len(self.names) % self.num_of_teams
        for count in range(1, num_of_teams + 1):
            self.teams.append([])

    def sort_teams(self):  # returns a 2D list of the teams
        shuffle(self.names)
        for i in range(self.num_of_teams):
            for j in range(self.players_per_team):
                name = self.names.pop(0)
                self.teams[i].append(name)

        team = 0
        for i in range(self.remaining_players):
            if team != self.remaining_players:
                team += 1
            else:
                team = 1
            shuffle(self.names)
            name = self.names.pop(0)
            team_index = team - 1
            self.teams[team_index].append(name)

        return self.teams
