# Teams Improved
## About:
Teams improved is an improved version of my teams script I used to use on my discord bot before I began to rewrite it completely. This script will allow a choice of the number of teams and also be slightly more efficient as before each team was stored on its own variable which is quite inefficient and also not scalable. The other team script was a test to see what I could do, and was not meant for use however I used to anyway out of laziness and only now I have decided to finally write one which is efficient and also more scalable.
## Use:
You are free to use and take any code here you like, no need to ask.
