from teams import Teams


class Main:

    def __init__(self):
        self.names = []
        self.num_of_teams = int(input("Please enter the number of teams you would like:\n"))
        while True:
            name = input("Please enter a username you would like to sort into teams:\n")
            while True:
                another = input("Would you like to add another username: (y/n)\n")
                if another == "y" or another == "n":
                    break
            self.names.append(name)
            if another == "n":
                break

    def sort_into_teams(self):
        teams = Teams(self.num_of_teams, self.names)
        sorted_teams = teams.sort_teams()
        print(sorted_teams)


if __name__ == "__main__":
    main = Main()
    main.sort_into_teams()
